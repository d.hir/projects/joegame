CC = gcc
CFLAGS = -Wall
LDFLAGS =
OBJDIR = obj
SRCDIR = src
BINDIR = bin
TESTDIR = tests
vpath %.c $(SRCDIR)
vpath %.h $(SRCDIR)
EXEC = joegame
VALGRINDTMPFILE = valgrindoutput.tmp

TESTFILES = $(wildcard $(TESTDIR)/*)
TESTTESTFILES = $(TESTFILES:$(TESTDIR)/%=TEST_%)
MEMCHECKTESTFILES = $(TESTFILES:$(TESTDIR)/%=MEMCHECK_%)
SOURCES = $(wildcard $(SRCDIR)/*.c)
SRCINOBJFOLDER = $(SOURCES:$(SRCDIR)%=$(OBJDIR)%)
OBJECTS = $(SRCINOBJFOLDER:.c=.o)

$(EXEC): $(OBJECTS)
	@mkdir -p $(BINDIR) $(OBJDIR)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: %.c
	$(CC) -c $(CFLAGS) $(DFLAGS) $< -o $@

.PHONY: new
new: clean $(EXEC)

.PHONY: test $(TESTTESTFILES)
test: $(EXEC) $(TESTTESTFILES)
$(TESTTESTFILES): 
	@echo "\n\e[33;1mTESTING $(@:TEST_%=%):\e[0m"
	$(BINDIR)/$(EXEC) < $(@:TEST_%=$(TESTDIR)/%)
	@echo

.PHONY: memcheck $(MEMCHECKTESTFILES)
memcheck: $(EXEC) rmvalgrindfile $(MEMCHECKTESTFILES)
	@echo "\n\e[33;1;4mMEM-CHECK SUMMARY\e[0m\n"
	@cat $(VALGRINDTMPFILE) | grep "ERROR SUMMARY\|MEM-CHECKING"
$(MEMCHECKTESTFILES): 
	@echo "\n\n\e[33;1mMEM-CHECKING $(@:MEMCHECK_%=%):\e[0m" \
			>> $(VALGRINDTMPFILE)
	valgrind --leak-check=full $(BINDIR)/$(EXEC) \
			< $(@:MEMCHECK_%=$(TESTDIR)/%) >> $(VALGRINDTMPFILE) 2>&1

.PHONY: clean
clean: 
	rm -f $(OBJECTS) $(BINDIR)/* $(VALGRINDTMPFILE)

.PHONY: rmvalgrindfile
rmvalgrindfile:
	rm -f $(VALGRINDTMPFILE)