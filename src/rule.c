#include "rule.h"
#include "io.h"
#include <stdlib.h>

static int global_rule_count = 0;

void init_rule(Rule* r) {
    r->text = (char*) malloc(sizeof(char) * INIT_RULE_LENGTH);
    r->len = 0;
    r->id = global_rule_count;
    global_rule_count++;
}

void delete_rule(Rule* r) {
    free(r->text);
}