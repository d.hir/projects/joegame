#include "board.h"
#include "field.h"
#include <stdlib.h>

static void setup_neighbours(Board* b) {
    for (int col = 0; col < b->dimx; col++) {
        for (int row = 0; row < b->dimy; row++) {
            get_field(b, col, row)->neighbours[0] = get_field(b, col + 1, row);
            get_field(b, col, row)->neighbours[1] = get_field(b, col, row + 1);
            get_field(b, col, row)->neighbours[2] = get_field(b, col - 1, row);
            get_field(b, col, row)->neighbours[3] = get_field(b, col, row - 1);
        }
    }
}

void init_board(Board* b, int dimx, int dimy) {
    b->dimx = dimx;
    b->dimy = dimy;
    b->size = dimx * dimy;
    b->fields = (Field*) malloc(sizeof(Field) * b->size);
    for (int j = 0; j < b->size; j++)
        init_field(&b->fields[j]);
    setup_neighbours(b);
}

void delete_board(Board* b) {
    for (int j = 0; j < b->size; j++)
        delete_field(&b->fields[j]);
    free(b->fields);
}

Field* get_random_field(Board* b) {
    return &b->fields[rand() % b->size];
}

Field* get_field(Board* b, int x, int y) {
    x += b->dimx;
    y += b->dimy;
    x %= b->dimx;
    y %= b->dimy;
    return &b->fields[y * b->dimx + x];
}