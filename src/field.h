#ifndef FIELD_H
#define FIELD_H

#include "rule.h"

typedef struct Field Field;
typedef struct Player Player;

struct Field {
    Rule rule;
    int has_rule;
    int is_occupied;
    Field** neighbours;
};

void init_field(Field*);
void delete_field(Field*);
void field_add_rule(Player*);

#endif /* FIELD_H */