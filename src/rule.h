#ifndef RULE_H
#define RULE_H

#define INIT_RULE_LENGTH 50

typedef struct Rule {
    char* text;
    int len;
    int id;
} Rule;

void init_rule(Rule*);
void delete_rule(Rule*);

#endif /* RULE_H */