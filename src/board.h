#ifndef BOARD_H
#define BOARD_H

#include "field.h"

typedef struct Board {
    int dimx;
    int dimy;
    int size;
    Field* fields;
} Board;

void init_board(Board*, int, int);
void delete_board(Board*);
Field* get_random_field(Board*);
Field* get_field(Board*, int, int);

#endif /* BOARD_H */