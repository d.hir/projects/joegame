#include "game.h"
#include "board.h"
#include "player.h"
#include "utils.h"
#include "cirque.h"
#include "io.h"
#include "field.h"
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int demand_exit_flag = FALSE;

static void place_players(Game* g) {
    for (Player* p = g->player_array.players; p < g->player_array.players + 
            g->player_array.num_players; p++) {
        while (1) {
            Field* f = get_random_field(&g->board);
            if (f->is_occupied == FALSE) {
                p->position = f;
                f->is_occupied = TRUE;
                break;
            }
        }
    }
}

void init_game(Game* g, int dimx, int dimy) {
    srand(time(0));
    init_board(&g->board, dimx, dimy);
    init_players(&g->player_array);
    place_players(g);
    g->round = 0;
}

void delete_game(Game* g) {
    delete_board(&g->board);
    delete_playerarray(&g->player_array);
}

static void print_start(PlayerArray* pa) {
    io_print_separator();
    IO_PRINTF("Registered %d players:\n", pa->num_players);
    for (Player* p = pa->players; p < pa->players + pa->num_players; p++)
        IO_PRINTF(" > Player %d: %s\n", p->id, p->name);
    IO_PRINTF("Starting game!\n");
    io_print_separator();
}

static void print_turn(Game* g, Player* player) {
    IO_PRINTF("Round %d, Player %d: It's %s's turn!\n", 
            g->round + 1, player->id, player->name);
}

void play_game(Game* g) {
    if (demand_exit_flag == TRUE)
        return;
    print_start(&g->player_array);
    Cirque player_cirque;
    cirque_create(&player_cirque, g->player_array.players, 
            g->player_array.num_players, sizeof(Player));
    Player* current_player;
    while(demand_exit_flag == FALSE) {
        DEBUG_PRINT("Starting Loop");
        DEBUG_PRINT("Getting player");
        current_player = cirque_get(&player_cirque);
        DEBUG_PRINT("Printing player");
        print_turn(g, current_player);
        DEBUG_PRINT("Moving player");
        player_move(current_player, &g->board);
        DEBUG_PRINT("Checking if player has rule set");
        if (current_player->position->has_rule) {
            DEBUG_PRINT("Adding effect to player");
            player_add_rule(current_player, &current_player->position->rule);
            DEBUG_PRINT("Printing rule to player");
            io_added_rule(current_player);
            DEBUG_PRINT("Waiting...");
            io_wait();
        } else {
            DEBUG_PRINT("Getting new rule");
            field_add_rule(current_player);
        }
        if (cirque_next(&player_cirque) == 0 && demand_exit_flag == FALSE) {
            DEBUG_PRINT("Incrementing round");
            g->round++;
        }
    }
}