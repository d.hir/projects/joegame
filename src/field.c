#include "field.h"
#include "utils.h"
#include "player.h"
#include "io.h"
#include <stdlib.h>

void init_field(Field* f) {
    f->has_rule = FALSE;
    f->is_occupied = FALSE;
    init_rule(&f->rule);
    f->neighbours = (Field**) malloc(4 * sizeof(Field*));
}

void delete_field(Field* f) {
    delete_rule(&f->rule);
}

void field_add_rule(Player* player) {
    IO_PRINTF("%s stepped on an empty field!\n", player->name);
    player->position->rule.len = 
            io_scanprint("Enter a new rule", &player->position->rule.text);
    player->position->has_rule = TRUE;
}