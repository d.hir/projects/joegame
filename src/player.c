#include "player.h"
#include "utils.h"
#include "board.h"
#include "io.h"
#include "game.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static void init_player(Player* player, const char* name, int name_length, int id) {
    player->name = (char*) malloc(sizeof(char) * (name_length + 1));
    strcpy(player->name, name);
    player->name_length = name_length;
    player->num_active_rules = 0;
    player->id = id;
    player->active_rules = malloc(sizeof(Rule*));
    player->num_allocated_rules = 1;
    IO_PRINTF("Welcome %s!\n", player->name);
}

void init_players(PlayerArray* player_array) {
    int counter = 0;
    int name_length = 0;
    player_array->players = (Player*) malloc(sizeof(Player));
    char* name = NULL;
    int alloced_players = 1;
    while (1) {
        char playerstr[80];
        sprintf(playerstr, 
                "Enter name for Player %d (empty if no more players)", 
                counter + 1);
        name_length = io_scanprint(playerstr, &name);
        if (name_length != 0 && demand_exit_flag == FALSE) {
            if (counter == alloced_players) {
                player_array->players = 
                        realloc(player_array->players, sizeof(Player) * 
                        alloced_players * 2);
                alloced_players *= 2;
            }
            init_player(player_array->players + counter, name, name_length, counter - 1);
        } else {
            break;
        }
        counter++;
    }
    free(name);
    player_array->num_players = counter - 1;
}

void delete_player(Player* player) {
    DEBUG_PRINT("Deleting Player %s", player->name);
    free(player->name);
    free(player->active_rules);
}

void delete_playerarray(PlayerArray* player_array) {
    for (Player* p = player_array->players; p < player_array->players + 
            player_array->num_players; p++) {
        delete_player(p);
    }
    free(player_array->players);
}

void player_move(Player* player, Board* b) {
    player->position->is_occupied = FALSE;
    int random_direction = rand() % 4;
    player->position = player->position->neighbours[random_direction];
    player->position->is_occupied = TRUE;
}

void player_add_rule(Player* player, Rule* rule) {
    if (player->num_active_rules != 0) {
        DEBUG_PRINT("Checking if player %s already has rule %d", player->name, rule->id);
        DEBUG_PRINT("first rule id: %d", (*player->active_rules)->id);
        for (Rule* r = *player->active_rules; 
                r < *player->active_rules + player->num_active_rules; r++) {
            DEBUG_PRINT("checking rule %d against %d", r->id, rule->id);
            if (r->id == rule->id) {
                IO_PRINTF("%s already has this rule applied:\n > %s\n", player->name, rule->text);
                return;
            }
        }
    }
    if (player->num_active_rules == player->num_allocated_rules) {
        DEBUG_PRINT("Reallocate player active rules");
        player->active_rules = realloc(player->active_rules, 
                sizeof(Rule*) * 2 * player->num_allocated_rules);
        player->num_allocated_rules *= 2;
    }
    DEBUG_PRINT("Adding rule to list of player rules");
    player->active_rules[player->num_active_rules] = rule;
    player->num_active_rules++;
}