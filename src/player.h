#ifndef PLAYER_H
#define PLAYER_H

#include "field.h"
#include "board.h"
#include "rule.h"
#include <stdlib.h>

typedef struct Player {
    char* name;
    ssize_t name_length;
    Field* position;
    Rule** active_rules;
    int num_active_rules;
    int num_allocated_rules;
    int id;
} Player;

typedef struct PlayerArray {
    Player* players;
    int num_players;
} PlayerArray;

void init_players(PlayerArray*);
void delete_player(Player*);
void delete_playerarray(PlayerArray*);
void player_move(Player*, Board*);
void player_add_rule(Player*, Rule*);

#endif /* PLAYER_H */