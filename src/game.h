#ifndef GAME_H
#define GAME_H

#include "board.h"
#include "player.h"

extern int demand_exit_flag;

typedef struct Game {
    Board board;
    PlayerArray player_array;
    int round;
} Game;

void init_game(Game*, int, int);
void delete_game(Game*);
void play_game(Game*);

#endif /* GAME_H */