#include "game.h"

#include "io.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
  Game g;
  init_game(&g, 2, 3);
  play_game(&g);
  delete_game(&g);
  return 0;
}
