#include "cirque.h"
#include <stdlib.h>

void cirque_create(Cirque* cirque, void* data, int num_elements, 
        int size_of_element) {
    cirque->entities = malloc(sizeof(void*) * num_elements);
    cirque->malloced_length = num_elements;
    for (int j = 0; j < num_elements; j++)
        cirque->entities[j] = data + j * size_of_element / sizeof(void);
    cirque->length = num_elements;
    cirque->current_element_position = 0;
}

void cirque_append(Cirque* cirque, void* data) {
    if (cirque->length == cirque->malloced_length) {
        cirque->entities = realloc(cirque->entities, 
                sizeof(void*) * 2 * cirque->malloced_length);
        cirque->malloced_length *= 2;
    }
    cirque->entities[cirque->length] = data;
    cirque->length++;
}

void* cirque_step(Cirque* cirque) {
    int index_to_return = cirque->current_element_position;
    cirque_next(cirque);
    return cirque->entities[index_to_return];
}

void* cirque_get(Cirque* cirque) {
    return cirque->entities[cirque->current_element_position];
}

int cirque_next(Cirque* cirque) {
    if (cirque->current_element_position + 1 == cirque->length)
        cirque->current_element_position = 0;
    else
        cirque->current_element_position++;
    return cirque->current_element_position;
}

void cirque_delete(Cirque* cirque) {
    free(cirque->entities);
}