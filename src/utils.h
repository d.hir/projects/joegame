#ifndef UTILS_H
#define UTILS_H

#define FALSE 0
#define TRUE 1

#include <stdio.h>

#ifdef DEBUG
#define DEBUG_PRINT(str, ...) \
        fprintf(stderr, " (DEBUG) " str "\n", ##__VA_ARGS__)
#else
#define DEBUG_PRINT(args...)
#endif

#endif