#include "io.h"
#include "utils.h"
#include "rule.h"
#include "player.h"
#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void io_print_separator_char(char c) {
    printf("\n");
    for (int j = 0; j < 80; j++)
        printf("%c", c);
    printf("\n\n");
}

void io_print_separator() {
    io_print_separator_char('#');
}

static void io_handle_command(char** cmd, int len) {
    char* subcmd = strtok(*cmd, " ");
    DEBUG_PRINT("Encountered special command: \"%s\"", subcmd);
    if (strcmp(subcmd + 1, "exit") == 0) {
        demand_exit_flag = TRUE;
        DEBUG_PRINT("Exiting on next possible occasion...");
    } else {
        IO_PRINTF("ERROR: Command not understood: \"%s\"\n", subcmd);
    }
}

int io_scanprint(const char* printstr, char** readstr) { 
    printf("%s> ", printstr);
    size_t str_byte_size = 0;
    int raw_line_length = getline(readstr, &str_byte_size, stdin);
    (*readstr)[raw_line_length - 1] = '\0';
    DEBUG_PRINT("io_scanprint got %s", *readstr);
    if ((*readstr)[0] == '$')
        io_handle_command(readstr, raw_line_length);
    return raw_line_length - 1;
}

void io_added_rule(Player* player) {
    printf("%s stepped on a field with an active rule:\n", player->name);
    printf(" > %s\n", player->active_rules[player->num_active_rules - 1]->text);
    char plural_s = (player->num_active_rules >= 2) ? 's' : '\0';
    printf("%s has now %d active rule%c:\n", player->name, 
            player->num_active_rules, plural_s);
    for (int j = 0; j < player->num_active_rules; j++)
        printf(" > %s\n", player->active_rules[j]->text);
}

void io_wait() {
    char* input_unused = NULL;
    io_scanprint("Press [Enter] to continue", &input_unused);
}