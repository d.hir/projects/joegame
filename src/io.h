#ifndef IO_H
#define IO_H

#include "player.h"
#include "game.h"
#include <stdio.h>

int io_scanprint(const char*, char**);
void io_added_rule(Player*);
void io_get_new_rule(Player*);
void io_print_turn(Game*, Player*);
void io_print_start(PlayerArray*);
void io_wait(void);
void io_print_separator(void);
void io_print_separator_char(char);

#define IO_PRINTF(args...) printf(args)

#endif