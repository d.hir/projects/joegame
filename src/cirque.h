#ifndef QUEUE_H
#define QUEUE_H

typedef struct Cirque {
    void** entities;
    int length;
    int malloced_length;
    int current_element_position;
} Cirque;

void cirque_create(Cirque*, void*, int, int);
void cirque_append(Cirque*, void*);
void* cirque_step(Cirque*);
void* cirque_get(Cirque*);
int cirque_next(Cirque*);
void cirque_delete(Cirque*);

#endif